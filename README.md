# Installing KubeEdge on RiscV

This page will guide you through the steps required to create an Heterogeneous Edge-Cloud Kubernetes-KubeEdge cluster with an x86/ARM cloud component and one/multiple RiscV edge compute nodes.
This guide will show how to install the following components:
|Component                      |Version                      |
|-------------------------------|-----------------------------|
|GoLang                         | `1.19.6`                    |
|runc                           | `1.1.4`                     |
|cri-o                          | `1.26.2`                    |
|conmon                         | `2.1.7`                     |
|cni-plugins                    | `1.20`                      | 
|KubeEdge                       | `1.13.0`                    |
|EdgeMesh                       | `1.13.0`                    |

Requirements:
 - An x86/ARM device with a working Docker environment to cross-compile KubeEdge and EdgeMesh container images;
 - A working Kubernetes Cluster.

Note: some of the listed commands might require sudo.

## Install GoLang

GoLang needs to be installed on both the x86/ARM and RiscV hosts to compile and install some of the components.
On the RiscV host, install GoLang 1.16.0 with apt install, which is required to bootstrap GoLang 1.19.6.
```bash
apt install golang
```
Download, compile and install GoLang 1.19.6:
```bash
wget https://go.dev/dl/go1.19.6.src.tar.gz
tar -xvf go1.19.6.src.tar.gz
cd go && ./all.bash
```
Remember to add the go binaries to your path. See the official documentation for more details: [GoLang](https://go.dev/doc/install/source).

GoLang should be installed on the x86/ARM host as well. Pre-compiled binaries are available, see [here](https://go.dev/doc/install).

## Install runc

Runc is required to execute container. To install it, simply download the source code, compile and install:
```bash
wget https://github.com/opencontainers/runc/archive/refs/tags/v1.1.4.tar.gz
tar -xvf v1.1.4.tar.gz
cd runc-1.1.4
apt install libseccomp-dev -y
make
make install
```
For more information: [GitHub page for runc](https://github.com/opencontainers/runc)

## Install cri-o
Cri-o acts as the interface between Kubernetes/KubeEdge and runc. It can be replaced by containerd or Docker, but it is not suggested as they require additional computational resources, which may not be available on a RiscV edge-computing board.
```bash
wget https://github.com/cri-o/cri-o/archive/refs/tags/v1.26.2.tar.gz
cd cri-o-1.26.2
apt install -y \
  libbtrfs-dev \
  git \
  libassuan-dev \
  libdevmapper-dev \
  libglib2.0-dev \
  libc6-dev \
  libgpgme-dev \
  libgpg-error-dev \
  libseccomp-dev \
  libsystemd-dev \
  libselinux1-dev \
  pkg-config \
  go-md2man \
  libudev-dev \
  software-properties-common \
  gcc
make
make install
make install.config
```
Install conmon:
```bash
git clone https://github.com/containers/conmon
cd conmon
make
make install
```
Override the default registry for the pause image in the cri-o configuration located in `/etc/crio/crio.conf`
```
[crio.image]
pause_image = "your_registry.com/pause:3.6"
# To use this project's pause image, you can insert this registry:
# registry.gitlab.com/parco-lab/kubeedge-v
```
Copy the basic bridge configuration into `/etc/cni/net.d`:
```bash
wget https://github.com/cri-o/cri-o/blob/main/contrib/cni/11-crio-ipv4-bridge.conflist
cp 11-crio-ipv4-bridge.conflist /etc/cni/net.d
```
Start and enable the cri-o daemon:
```bash
systemctl daemon-reload
systemctl enable crio
systemctl start crio
```
For more information: [GitHub installation page for cri-o](https://github.com/cri-o/cri-o/blob/main/install.md)

## Install CNI-Plugins

To install the CNI-Plugins, you can download and install the pre-compiled v1.20 binaries for RiscV from [here](https://github.com/containernetworking/plugins/releases/tag/v1.2.0).
```bash
wget https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-riscv64-v1.2.0.tgz
mkdir ./cni-plugins-1.20
tar -xvf cni-plugins-linux-riscv64-v1.2.0.tgz -C ./cni-plugins-1.20
cp ./cni-plugins-1.20/* /opt/cni/bin/
```
For more information: [GitHub containernetworking/plugins](https://github.com/containernetworking/plugins)

# Compiling KubeEdge

To install KubeEdge we need to compile its executables, which will be needed to join the cluster. Then, create the Docker installation images used during the installation process.

## KubeEdge Executables
Clone the repository and compile the executables:
```bash
git clone https://github.com/kubeedge/kubeedge.git -b release-1.13
cd kubeedge
make BUILD_WITH_CONTAINER=false
```
In the `./_output/local/bin/`folder, you will find ten executables. Including `keadm` and `edgecore`, which will be required to join the existing Kubernetes cluster. Add these executables to your path or copy them in `/usr/local/bin`.

For more information: [KubeEdge official documentation](https://kubeedge.io/en/docs/setup/local/#build-from-source)

## The pause executable
The `pause` image needs the pause executable, which can be obtained by compiling `pause.c` from the [kubernetes-csi](https://github.com/kubernetes-csi) / **[driver-registrar](https://github.com/kubernetes-csi/driver-registrar)** repository:
```bash
git clone https://github.com/kubernetes-csi/driver-registrar.git
cd ./driver-registrar/vendor/k8s.io/kubernetes/build/pause
```
The `Makefile` uses the builder image `k8s.gcr.io/kube-cross` which does not include `riscv64-linux-gnu-gcc`. Thus, we need to manually compile the executable:
```bash
# apt install riscv64-linux-gnu-gcc # If you do not already have the gcc RiscV cross-compiler 
riscv64-linux-gnu-gcc -Os -Wall -Werror -static -o pause-riscv64 pause.c
```

## EdgeMesh Container images

As KubeEdge only supports EdgeMesh as network plugin, we need to create a container image that can be pulled while deploying it in Kubernetes. To do so, clone the EdgeMesh repository and compile to generate the x86/ARM executables. For x86:
```bash
git clone https://github.com/kubeedge/edgemesh.git -b release-1.13
cd edgemesh
go mod tidy -go=1.18
make
mv ./_output/local/bin/edgemesh-agent ./_output/local/bin/edgemesh-agent-amd64
```
To compile for RiscV, modify the file: `./hack/lib/golang.sh` at line **223** as follows:
```bash
GOOS=linux GOARCH=riscv64 go build -o ${EDGEMESH_OUTPUT_BINPATH}/${name} -gcflags="${gogcflags:-}" -ldflags "${goldflags:-}" $bin
```
Then run:
```bash
touch ./vendor/github.com/marten-seemann/tcp/sys_linux_riscv64.go
echo 'package tcp

const (
        sysSIOCINQ  = 0x541b
        sysSIOCOUTQ = 0x5411
)' > ./vendor/github.com/marten-seemann/tcp/sys_linux_riscv64.go
make
mv ./_output/local/bin/edgemesh-agent ./_output/local/bin/edgemesh-agent-riscv64
```
Finally, on the RiscV host, run:
```bash
sysctl -w net.core.rmem_max=2500000
```

## KubeEdge Container installation images
When joining the Kubernetes cluster, KubeEdge downloads a sequence of seven docker images:
> kubeedge/iptables-manager:v1.13.0 \
kubeedge/controller-manager:v1.13.0 \
kubeedge/installation-package:v1.13.0 \
kubeedge/eclipse-mosquitto:1.6.15 \
kubeedge/pause:3.6 \
kubeedge/admission:v1.13.0 \
kubeedge/cloudcore:v1.13.0

All images are needed, except for mosquitto, which is available in apt and can be installed separetely with:
```bash
apt install mosquitto -y
```
To create the Docker images, clone this repository on the x86/ARM host, copy the ten KubeEdge executables from the RiscV host into the bin directory. You also need to copy `pause`, `edgemesh-agent-amd64` and `edgemesh-agent-riscv64` executables:
```bash
git clone REPO_NAME_MISSING
cd REPO_NAME_MISSING
cp /path/to/executables/* ./bin/
REPO=your_registry.com ./build.sh
```
This script will automatically create the containers required by KubeEdge.

Note: the `bin` directory contains pre-compiled executables for RiscV64, for maximum compatibility, we recommend using the executables from your compilation of KubeEdge.

# Installing KubeEdge
### CloudCore
After setting up the Kubernetes cluster (start [here](https://kubernetes.io/docs/setup/)), setup the KubeEdge CloudCore by following the KubeEdge [documentation](https://kubernetes.io/docs/setup/) normally. Do not install a network plugin like Flannel or Calico in Kubernetes, as they are not supported.

### EdgeCore
To install the EdgeCore, either move to the `keadm` executable folder, or add it to the path.
Run the join command with the parameters for the custom repository where you pushed the custom RiscV container images or use this repository:
```bash
# REPO=registry.gitlab.com/parco-lab/kubeedge-v
REPO=your_registry.com
sudo keadm join --cloudcore-ipport=$CLOUD_IP:10000 --token=$TOKEN --kubeedge-version=v1.13.0 --remote-runtime-endpoint=unix:///var/run/crio/crio.sock --image-repository=$REPO
```
### EdgeMesh

Prepare to install EdgeMesh by following the prerequisites [here](https://edgemesh.netlify.app/). Then perform the steps for the manual installation on the cloud/master node of the cluster:
```bash
git clone https://github.com/kubeedge/edgemesh.git -b release-1.13
cd edgemesh
kubectl apply -f build/crds/istio/
```
Modify line 37 of file `./build/agent/resources/04-configmap.yaml` with:
```bash
openssl rand -base64 32
```
Then modify line 25 of file `edgemesh/build/agent/resources/05-daemonset.yaml` with:
```yaml
#registry.gitlab.com/parco-lab/kubeedge-v/edgemesh-agent:v1.13.0
your_registry.com/edgemesh-agent:v1.13.0
```
Finally:
```bash
kubectl apply -f build/agent/resources/
```

You now have a fully functional Kubernetes/KubeEdge installation on RiscV.
