#!/bin/bash

if [[ -z "${REPO}" ]]; then
  REPOSITORY="registry.gitlab.com/francescol96/kubernetes-cluster-images"
else
  REPOSITORY="${REPO}"
fi

cp ./bin/admission 				./images/admission
cp ./bin/cloudcore				./images/cloudcore
cp ./bin/controllermanager		./images/controller-manager
cp ./bin/edgecore				./images/installation-package
cp ./bin/keadm					./images/installation-package
cp ./bin/iptablesmanager		./images/iptables-manager
cp ./bin/pause					./images/pause
cp ./bin/edgemesh-agent-amd64	./images/edgemesh-agent-amd64/edgemesh-agent
cp ./bin/edgemesh-agent-riscv64	./images/edgemesh-agent-riscv64/edgemesh-agent

docker buildx build --platform=linux/riscv64 -t $REPOSITORY/admission:v1.13.0 				--load ./images/admission
docker buildx build --platform=linux/riscv64 -t $REPOSITORY/installation-package:v1.13.0 	--load ./images/installation-package
docker buildx build --platform=linux/riscv64 -t $REPOSITORY/cloudcore:v1.13.0 				--load ./images/cloudcore
docker buildx build --platform=linux/riscv64 -t $REPOSITORY/iptables-manager:v1.13.0 		--load ./images/iptables-manager
docker buildx build --platform=linux/riscv64 -t $REPOSITORY/controller-manager:v1.13.0 		--load ./images/controller-manager
docker buildx build --platform=linux/riscv64 -t $REPOSITORY/pause:3.6 						--load ./images/pause

docker buildx build --platform=linux/riscv64 -t $REPOSITORY/edgemesh-agent:v1.13.0-riscv64 	--load ./images/edgemesh-agent-riscv64
docker buildx build --platform=linux/amd64 	 -t $REPOSITORY/edgemesh-agent:v1.13.0-amd64 	--load ./images/edgemesh-agent-amd64

docker buildx imagetools create -t $REPOSITORY/edgemesh-agent:v1.13.0-riscv64 --append $REPOSITORY/edgemesh-agent:v1.13.0-amd64

rm ./images/admission/admission
rm ./images/cloudcore/cloudcore
rm ./images/controller-manager/controllermanager
rm ./images/installation-package/edgecore
rm ./images/installation-package/keadm
rm ./images/iptables-manager/iptablesmanager
rm ./images/pause/pause
rm ./images/edgemesh-agent-amd64/edgemesh-agent
rm ./images/edgemesh-agent-riscv64/edgemesh-agent
